$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "coordinates_converter_geotec/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "coordinates_converter_geotec"
  s.version     = CoordinatesConverterGeotec::VERSION
  s.authors     = ["Edgardo Martin Gerez"]
  s.email       = ["edmargerez@gmail.com"]
  s.homepage    = "http://geotecconsultora"
  s.summary     = "Gauss-Kruger Coord to LatLng and Viceversa"
  s.description = "This plugin uses https://www.linz.govt.nz/geodetic/conversion-coordinates/projection-conversions/transverse-mercator-preliminary-computations/index.aspx formulae and is based on https://github.com/cgcai/SVY21"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.1"

  s.add_development_dependency "sqlite3"
end
